from src.kafka.message_handler import MessageHandler
from src.settings import GENERAL
from datetime import datetime

from sentence_transformers import SentenceTransformer

import logging

log = logging.getLogger(__name__)


class Doc2VecMessageHandler(MessageHandler):

    def __init__(self):
        log.info("Initialize Doc2VecMessageHandler")
        self.doc2vec = SentenceTransformer('bert-base-nli-mean-tokens', device=GENERAL['COMPUTING_DEVICE'])

    async def handle(self, message):
        sentences = message['sentences']
        message['sent2vec'] = self.doc2vec.encode(sentences).tolist()
        message['Chronology'].append({
            'TransformerType': 'Doc2Vec',
            'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'TransformerVersion': '0.1'
        })
        log.debug(f'processed message {message}')
        return message
