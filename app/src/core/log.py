import logging
import os


def setup_logger(app_name, logging_level=logging.INFO):
    logging.basicConfig(
        level=int(logging_level),
        format="[%(asctime)s][%(process)d][%(thread)d]:" + logging.BASIC_FORMAT,
        handlers=[
            logging.StreamHandler()
        ]
    )
    log = logging.getLogger(__name__)
    log.info(f'logging level {logging_level}')
    log.info(f'[{app_name}] start console logger')