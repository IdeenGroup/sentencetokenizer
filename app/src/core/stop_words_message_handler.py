from src.kafka.message_handler import MessageHandler
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from string import punctuation
from datetime import datetime


class StopWordMessageHandler(MessageHandler):

    def __init__(self):
        self.stop_words = set(stopwords.words('english'))
        self.stop_words |= set(punctuation)

    def remove_stop_words(self, sent):
        return ' '.join([word for word in word_tokenize(sent) if word not in self.stop_words])

    async def handle(self, message):
        message['sentences_stop_word'] = [self.remove_stop_words(sent) for sent in message['sentences']]
        message['Chronology'].append({
            'TransformerType': 'StopWord',
            'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'TransformerVersion': '0.1'
        })
        return message