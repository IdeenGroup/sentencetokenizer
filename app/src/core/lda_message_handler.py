import gensim
import gensim.corpora as corpora
from gensim.parsing.preprocessing import preprocess_string, strip_punctuation, strip_numeric
from datetime import datetime

from src.kafka.message_handler import MessageHandler


def sent_to_words(sentences):
    for sentence in sentences:
        yield (gensim.utils.simple_preprocess(str(sentence), deacc=True))


class LdaMessageHandler(MessageHandler):

    async def handle(self, message):
        stems = message['sentences_stemming']
        num_topics = 5
        num_words = 3
        words = list(sent_to_words(stems))
        id2word = corpora.Dictionary(words)
        corpus = [id2word.doc2bow(text) for text in words]
        lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                                    id2word=id2word,
                                                    num_topics=num_topics)
        lda_topics = lda_model.show_topics(num_words=num_words)
        topics = []
        filters = [lambda x: x.lower(), strip_punctuation, strip_numeric]
        for topic in lda_topics:
            topics.append(preprocess_string(topic[1], filters))
        topics_dict = {}
        for i, topic in enumerate(topics):
            topics_dict["Theme %d:" % (i + 1)] = topic
        message['topics'] = [topics_dict]
        message['Chronology'].append({
            'TransformerType': 'Lda',
            'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'TransformerVersion': '0.1'
        })
        return message
