import gensim.downloader as api
from datetime import datetime
import keras
import logging
import numpy as np
from nltk.tokenize import word_tokenize

from src.kafka.message_handler import MessageHandler
from src.settings import GENERAL

log = logging.getLogger(__name__)


class PsyEffectsMaterialMessageHandler(MessageHandler):

    def __init__(self):
        log.info(f'mode: {GENERAL["MODE"]}')
        self.materials_model = keras.models.load_model(GENERAL['PATH_TO_MODEL'])
        self.word2vec = api.load("glove-wiki-gigaword-50")

    async def handle(self, message):

        sentences = message['sentences']
        prob_threshold = GENERAL['PROB_LIMIT']
        materials_words = []
        for i, sentence in enumerate(sentences):
            words = word_tokenize(sentence)
            words = map(str.lower, words)
            word2vec = {}
            for word in words:
                if word in self.word2vec:
                    word2vec[word] = self.word2vec[word]
            word_vecs = np.array(list(word2vec.values()))
            word_keys = np.array(list(word2vec.keys()))
            probs = self.materials_model.predict(word_vecs).reshape(1, -1)[0]

            materials_words += word_keys[np.where(probs > prob_threshold)].tolist()

        message['material_words'] = materials_words
        message['Chronology'].append({
            'TransformerType': f'{GENERAL["MODE"].capitalize()}',
            'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'TransformerVersion': '0.1'
        })
        log.info('message was handled')
        return message
