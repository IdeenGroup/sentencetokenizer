from src.kafka.message_handler import MessageHandler
from nltk.tokenize import sent_tokenize
from datetime import datetime
import logging

log = logging.getLogger(__name__)


class TokenizeMessageHandler(MessageHandler):

    def __init__(self):
        pass

    async def handle(self, message):
        ulid = message['ULID']
        title = message['title']
        author_first_name = message['author_first_name']
        author_last_name = message['author_last_name']
        author_country = message['author_country']
        abstract = message['abstract']
        full_text = message['description']
        chronology = message['Chronology']

        chronology.append({
            'TransformerType': 'Tokenizer',
            'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'TransformerVersion': '0.2'
        })

        sentences = sent_tokenize(full_text)
        log.info('message handled')
        return {
            'ULID': ulid,
            'Chronology': chronology,
            'title': title,
            'author_first_name': author_first_name + ' ' +author_last_name,
            'author_country': author_country,
            'text': full_text,
            'abstract': abstract,
            'sentences': sentences
        }