from src.kafka.message_handler import MessageHandler
from src.settings import GENERAL
from datetime import datetime

import numpy as np
from scipy.spatial.distance import cdist
from sklearn.cluster import KMeans

import logging
log = logging.getLogger(__name__)


def closest_node(node, nodes):
    index = cdist([node], nodes).argmin()
    return nodes[index], index


def get_true_index(index, key, labels):
    i = 0
    for label in labels:
        i += 1
        if key == label:
            if index > 0:
                index -= 1
            else:
                break
    return i


def get_centers(sent, v, labels):
    log.info("Getting main ideas")
    n_clasters = max(labels) + 1
    main_ideas = []
    for i in range(n_clasters):
        mask = labels == i
        some_vecs = np.array(v)[mask]
        cluster_center = np.mean(some_vecs, axis=0)
        center, index = closest_node(cluster_center, some_vecs)
        true_index = get_true_index(index, i, labels)
        main_ideas.append({"cluster_size": len(some_vecs),
                           "center": {"number": true_index,
                                      "text of sentence": sent[true_index]
                                     },
                          })
    return main_ideas


def handle_kmean(v, n_clusters=3):
    log.info("KMean processing")
    model = KMeans(n_clusters=n_clusters)
    model.fit(v)
    all_predictions = model.predict(v)
    return all_predictions


class MainIdeasMessageHandler(MessageHandler):
    async def handle(self, message):
        sentences = message['sentences']
        vecs = message['sent2vec']
        labels = handle_kmean(vecs, n_clusters=3)
        message['main_ideas'] = get_centers(sentences, vecs, labels)
        message['Chronology'].append({
            'TransformerType': 'Main ideas',
            'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'TransformerVersion': '0.1'
        })
        log.debug(f'processed message {message}')
        return message
