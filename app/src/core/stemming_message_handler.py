from nltk import PorterStemmer
from datetime import datetime

from src.kafka.message_handler import MessageHandler


class StemmingMessageHandler(MessageHandler):

    def __init__(self):
        self.porter = PorterStemmer()

    async def handle(self, message):
        sentences = message['sentences_stop_word']
        message['sentences_stemming'] = [self.porter.stem(sent) for sent in sentences]
        message['Chronology'].append({
            'TransformerType': 'Stemming',
            'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'TransformerVersion': '0.1'
        })
        return message