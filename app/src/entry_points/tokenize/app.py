from src.core.tokenize_message_handler import TokenizeMessageHandler
from src.kafka.kafka_manager import StraightKafkaManager
from src.settings import KAFKA


class App:

    def __init__(self):
        self.kafka_manager = StraightKafkaManager(
            bootstrap_servers=KAFKA['BOOTSTRAP_SERVER'],
            input_topic=KAFKA['INPUT_TOPIC'],
            output_topic=KAFKA['OUTPUT_TOPIC'],
            consumer_group=KAFKA['CONSUMER_GROUP'],
            message_handler=TokenizeMessageHandler()
        )

    def run(self):
        self.kafka_manager.run()