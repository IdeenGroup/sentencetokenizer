from src.core.main_ideas_message_handler import MainIdeasMessageHandler
from src.kafka.kafka_manager import StraightKafkaManager
from src.settings import KAFKA


class App:

    def __init__(self):
        self.kafka_manager = StraightKafkaManager(
            bootstrap_servers=KAFKA['BOOTSTRAP_SERVER'],
            input_topic=KAFKA['INPUT_TOPIC'],
            output_topic=KAFKA['OUTPUT_TOPIC'],
            consumer_group=KAFKA['CONSUMER_GROUP'],
            message_handler=MainIdeasMessageHandler()
        )

    def run(self):
        self.kafka_manager.run()
