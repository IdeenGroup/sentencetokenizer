import traceback
from typing import List

import aiokafka
import logging
import json

from src.kafka.kafka_sender import KafkaSender
from src.kafka.message_handler import MessageHandler

log = logging.getLogger(__name__)


class KafkaListener:

    __slots__ = 'bootstrap_servers', 'input_topic', 'consumer_group', 'kafka_sender', 'message_handler', 'consumer'

    def __init__(self,
                 bootstrap_servers: List[str],
                 input_topic: str,
                 consumer_group: str,
                 message_handler: MessageHandler,
                 kafka_sender: KafkaSender):
        log.info(f"read messages from kafka: {bootstrap_servers}/{input_topic} as {consumer_group}")
        self.bootstrap_servers = bootstrap_servers
        self.input_topic = input_topic
        self.consumer_group = consumer_group
        self.kafka_sender = kafka_sender
        self.message_handler = message_handler

    async def start(self):
        self.consumer = aiokafka.AIOKafkaConsumer(
            self.input_topic,
            bootstrap_servers=self.bootstrap_servers,
            group_id=self.consumer_group,
            enable_auto_commit=False,
            auto_commit_interval_ms=0,
            auto_offset_reset="earliest",
        )
        await self.consumer.start()
        try:
            async for message in self.consumer:
                await self.consumer.commit()
                log.debug('message received')
                parsed_message = json.loads(message.value.decode('utf-8'))
                try:
                    output = await self.message_handler.handle(message=parsed_message)
                    await self.kafka_sender.send(output=output)
                except Exception as e:
                    traceback_msg = str(traceback.format_exc())
                    log.exception(traceback_msg)
        finally:
            await self.consumer.stop()

