import asyncio
from typing import List

from src.kafka.kafka_listener import KafkaListener
from src.kafka.kafka_sender import KafkaSender
from src.kafka.message_handler import MessageHandler

import logging

log = logging.getLogger(__name__)


class StraightKafkaManager:

    def __init__(self,
                 bootstrap_servers: List[str],
                 input_topic: str,
                 output_topic: str,
                 consumer_group: str,
                 message_handler: MessageHandler):

        self.kafka_sender = KafkaSender(
            bootstrap_servers=bootstrap_servers,
            output_topic=output_topic
        )
        self.kafka_listener = KafkaListener(
            bootstrap_servers=bootstrap_servers,
            input_topic=input_topic,
            consumer_group=consumer_group,
            message_handler=message_handler,
            kafka_sender=self.kafka_sender
        )

    def run(self):
        ioloop = asyncio.get_event_loop()
        tasks = [
            ioloop.create_task(self.kafka_sender.start()),
            ioloop.create_task(self.kafka_listener.start()),
        ]
        ioloop.run_until_complete(asyncio.wait(tasks))
        ioloop.close()