import traceback
import logging
import aiokafka
from json import dumps
from uuid import uuid4

from src.settings import KAFKA

log = logging.getLogger(__name__)


class KafkaSender:

    __slots__ = 'producer', 'output_topic', 'bootstrap_servers',

    def __init__(self,
                 bootstrap_servers,
                 output_topic):
        log.info(f"send messages to kafka: {bootstrap_servers}/{output_topic}")
        self.output_topic = output_topic
        self.bootstrap_servers = bootstrap_servers

    async def start(self):
        log.info("start async cycle")
        log.info(f"max size of msg byte {KAFKA['MESSAGE_SIZE_BYTE']}")
        self.producer = aiokafka.AIOKafkaProducer(
            bootstrap_servers=self.bootstrap_servers,
            max_request_size=KAFKA['MESSAGE_SIZE_BYTE']
        )
        await self.producer.start()

    async def send(self, output: dict):
        try:
            data = dumps(output, ensure_ascii=False).encode('utf-8')
            await self.producer.send(self.output_topic, data, key=str.encode(output['ULID']))
            log.debug(f"send msg to {self.output_topic}")
        except:
            traceback_msg = str(traceback.format_exc())
            log.exception(traceback_msg)