from abc import ABC, abstractmethod


class MessageHandler(ABC):

    @abstractmethod
    async def handle(self, message):
        pass