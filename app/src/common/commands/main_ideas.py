from src.core.log import setup_logger
from src.entry_points.main_ideas.app import App
from src.settings import GENERAL


def do():
    setup_logger('main_ideas', GENERAL['LOGGING_LEVEL'])
    App().run()
