from src.core.log import setup_logger
from src.entry_points.material.app import App
from src.settings import GENERAL


def do():
    setup_logger('Material', GENERAL['LOGGING_LEVEL'])
    App().run()
