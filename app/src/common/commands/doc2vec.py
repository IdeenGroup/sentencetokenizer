from src.core.log import setup_logger
from src.entry_points.doc2vec.app import App
from src.settings import GENERAL


def do():
    setup_logger('doc2vec', GENERAL['LOGGING_LEVEL'])
    App().run()
