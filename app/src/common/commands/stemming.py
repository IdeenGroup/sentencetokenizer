from src.core.log import setup_logger
from src.entry_points.stemming.app import App
from src.settings import GENERAL


def do():
    setup_logger('stemming', GENERAL['LOGGING_LEVEL'])
    App().run()