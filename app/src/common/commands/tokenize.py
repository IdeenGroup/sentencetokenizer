from src.core.log import setup_logger
from src.entry_points.tokenize.app import App
from src.settings import GENERAL


def do():
    setup_logger('tokenize', GENERAL['LOGGING_LEVEL'])
    App().run()