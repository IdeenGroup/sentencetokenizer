from src.core.log import setup_logger
from src.entry_points.stop_word.app import App
from src.settings import GENERAL


def do():
    setup_logger('stop_word', GENERAL['LOGGING_LEVEL'])
    App().run()