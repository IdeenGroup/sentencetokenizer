from src.core.log import setup_logger
from src.entry_points.lda.app import App
from src.settings import GENERAL


def do():
    setup_logger('lda', GENERAL['LOGGING_LEVEL'])
    App().run()
