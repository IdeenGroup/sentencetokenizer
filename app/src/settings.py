import os


GENERAL = {
    'LOGGING_LEVEL': int(os.getenv('LOGGING_LEVEL', '20')),
    'COMPUTING_DEVICE': os.getenv('COMPUTING_DEVICE', 'cpu'),
    'MODE': os.getenv('MODEL', 'materials'),
    'PATH_TO_MODEL': os.path.join(
        os.path.abspath(os.path.dirname(__file__)),
        '../res/models/',
        'materials_metric/Epo10_D50D10DrO0.2' if os.getenv('MODEL', 'materials') == 'materials' else 'physical_effects_metric/Epo10_D50D10DrO0.2'
    ),
    'PROB_LIMIT': 0.85
}

KAFKA = {
    'BOOTSTRAP_SERVER': os.getenv('KAFKA_BOOTSTRAP_SERVER', 'localhost:9092').split(','),
    'INPUT_TOPIC': os.getenv('KAFKA_INPUT_TOPIC', 'patents-6-main-ideas'),
    'OUTPUT_TOPIC': os.getenv('KAFKA_OUTPUT_TOPIC', 'patents-7-material_words'),
    'CONSUMER_GROUP': os.getenv('KAFKA_CONSUMER_GROUP', 'ideen-7'),
    'MESSAGE_SIZE_BYTE': int(os.getenv('KAFKA_MESSAGE_SIZE_BYTE', "100000000"))
}

MGMT_COMMANDS_ALIASES = {
    'tokenize': 'src.common.commands.tokenize',
    'stop_words': 'src.common.commands.stop_words',
    'stemming': 'src.common.commands.stemming',
    'lda': 'src.common.commands.lda',
    'doc2vec': 'src.common.commands.doc2vec',
    'main_ideas': 'src.common.commands.main_ideas',
    'material_phy_effects': 'src.common.commands.material_phy_effects',
}
