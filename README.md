# Kafka Wrapper

Kafka wrapper is a pattern project for implement reading from one topic, transfrom data and send to another without any interaction with kafka

## Docker
You can run this application in docker
### Prerequirements
Clone this project
### Envirement variables
You can you use `.env` file as it or change itself with your needs
### Build
Build docker image
```
docker-compose build
```
### Run
Run docker containers
```
docker-compose up -d
```

